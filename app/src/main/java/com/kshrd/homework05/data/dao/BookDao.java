package com.kshrd.homework05.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.kshrd.homework05.data.entity.Book;

import java.util.List;

@Dao
public interface BookDao {
    @Insert
    void save(Book book);
    @Insert
    void save(Book... books);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(List<Book> books);
    @Delete
    void delete(Book book);

    @Query("delete from book where id = :id")
    void deleteBook(int id);
    @Update
    void update(Book book);
    @Query("SELECT * FROM book ORDER BY id ASC")
    List<Book>getBooks();

    @Query("UPDATE book SET title= :title,size= :size,price= :price WHERE 'key'= :key")
    void updateBook(String title,String size,String price,int key);

}
