package com.kshrd.homework05.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.kshrd.homework05.data.dao.BookDao;
import com.kshrd.homework05.data.entity.Book;


@Database(
        version = 1,
        entities = { Book.class}
)
public abstract class MyDatabase extends RoomDatabase {
    static final String DB_Name="book_db";

    public abstract BookDao bookDao();
    private static MyDatabase instance;

    public static  MyDatabase getInstance(final Context context){
//        return Room.databaseBuilder(context,
//                MyDatabase.class,DB_Name)
//                .allowMainThreadQueries()
//                .build();
        if(instance==null){
            synchronized (MyDatabase.class){
                instance=Room.databaseBuilder(context.getApplicationContext(),
                        MyDatabase.class,"DB_Name")
                        .allowMainThreadQueries()
                        .build();
            }

        }
        return instance;
    }

}
