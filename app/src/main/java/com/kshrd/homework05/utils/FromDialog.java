package com.kshrd.homework05.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kshrd.homework05.R;
import com.kshrd.homework05.data.MyDatabase;
import com.kshrd.homework05.data.entity.Book;


public class FromDialog extends DialogFragment {
    EditText bookName,sizeImg,price;
    Spinner spinner;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState){
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("create book");
        View view= LayoutInflater.from(getActivity())
                .inflate(R.layout.add_book_from_dialog,null);
        builder.setView(view);
        bookName=view.findViewById(R.id.txtTitle);
        spinner=view.findViewById(R.id.spinnerType);
        sizeImg=view.findViewById(R.id.txtSize);
        price=view.findViewById(R.id.txtPrice);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title=bookName.getText().toString().trim();
                String size=sizeImg.getText().toString().trim();
                String priceBook=price.getText().toString().trim();
                Book bookModel = new Book();
                bookModel.setTitle(title);
                bookModel.setPrice(priceBook);
                bookModel.setSize(size);
                MyDatabase.getInstance(getContext()).bookDao().save(bookModel);
                bookName.setText("");
                sizeImg.setText("");
                price.setText("");
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //System.exit(0);
                bookName.setText("");
                sizeImg.setText("");
                price.setText("");
            }
        });

        return builder.create();
    }

    public DialogCallback callback;
    public void setCallback(DialogCallback callback) {
        this.callback = callback;
    }

    public interface DialogCallback<T>{
        void onClicked(T data);
    }
}
