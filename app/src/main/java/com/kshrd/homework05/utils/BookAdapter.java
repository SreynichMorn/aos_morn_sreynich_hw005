package com.kshrd.homework05.utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;


import com.kshrd.homework05.R;
import com.kshrd.homework05.ReadActivity;
import com.kshrd.homework05.data.MyDatabase;
import com.kshrd.homework05.data.dao.BookDao;
import com.kshrd.homework05.data.entity.Book;

import java.util.ArrayList;
import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder>{

public List<Book>books;
Context context;
ImageView bookOption;
Delete deleteBook;

    public BookAdapter(Context context, List<Book> books, FragmentManager fragmentManager) {
        this.books = books;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item,parent,false);
        return new ViewHolder(view);
    }

    public void addMoreItem(List<Book>books){
        int previousSize=this.books.size();
        this.books.addAll(books);
        notifyItemRangeInserted(previousSize,books.size());
    }

    public void reloadItems(List<Book>books){
        this.books.clear();
        this.books.addAll(books);
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bind(books.get(position));
        holder. bookOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context,v);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    FragmentManager fragmentManager;

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.remove:
                                Toast.makeText(context, "Delete Successfully", Toast.LENGTH_SHORT).show();
                                deleteBook.onBookDelete(position,books.get(position).getId());
//                                deleteBook.getAppDatabase(context).bookDao().deleteBook(books.get(position).());
                                return true;
                            case R.id.update:
                                Bundle args = new Bundle();
                                    args.putString("id",String.valueOf(books.get(position).getId()));
                                    args.putString("title",String.valueOf(books.get(position).getTitle()));
                                    args.putString("size",String.valueOf(books.get(position).getSize()));
                                    args.putString("price",String.valueOf(books.get(position).getPrice()));
                                    DialogFragment update_dialog = new UpdateDialog();
                                    update_dialog.setArguments(args);
                                    update_dialog.show(fragmentManager ,"TAG");
                                    return true;
                            case R.id.read:
                                Intent intent=new Intent(context, ReadActivity.class);
//
                                context.startActivity(intent);
                                return true;
                            default:
                                return false;
                        }
                    }

//
//
                });
                popupMenu.inflate(R.menu.option);
                popupMenu.show();
            }

        });

    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView title,sizeImg,price,typeBook;
        ImageView bookOption;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.txtTitle);
            typeBook=itemView.findViewById(R.id.txtType);
            sizeImg=itemView.findViewById(R.id.txtSize);
            price=itemView.findViewById(R.id.txtPrice);
            bookOption=itemView.findViewById(R.id.bookOption);

        }

        void bind(Book book){
            title.setText(book.title);
            typeBook.setText(book.type);
            sizeImg.setText( book.size);
            price.setText( book.price);
        }
    }

    public interface Delete{
        void onBookDelete(int position,int id);

        MyDatabase getAppDatabase(Context context);
    }


}
