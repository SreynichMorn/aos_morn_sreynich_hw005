package com.kshrd.homework05.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.kshrd.homework05.R;
import com.kshrd.homework05.data.MyDatabase;
import com.kshrd.homework05.data.entity.Book;

public class UpdateDialog extends DialogFragment {
    public static final String SimpleName=UpdateDialog.class.getSimpleName();

    EditText book_name,size_img,price_book;
    Spinner spinner;

    private Button update, positive, negative;
    private AlertDialog.Builder builder;

    private static UpdateDialog instance = new UpdateDialog();

    public static UpdateDialog getInstance(){
        return instance;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState){
        Bundle args = new Bundle();
        final AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("update book");
        View view= LayoutInflater.from(getActivity())
                .inflate(R.layout.add_book_from_dialog,null);
        builder.setView(view);
        book_name=view.findViewById(R.id.txtTitle);
        spinner=view.findViewById(R.id.spinnerType);
        size_img=view.findViewById(R.id.txtSize);
        price_book=view.findViewById(R.id.txtPrice);

//        bookName.setText();
        Bundle mArgs = getArguments();
        final String bookName = mArgs.getString("title");
        final String sizeImg = mArgs.getString("size");
        final String price = mArgs.getString("price");
//                String userScore = mArgs.getString("score");
        book_name.setText(bookName);
        size_img.setText(sizeImg);
        price_book.setText(price);

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                String title=bookName.getText().toString().trim();
//                String size=sizeImg.getText().toString().trim();
//                String priceBook=price.getText().toString().trim();


//                user_score.setText(userScore);
                if(bookName.equals("")||sizeImg.equals("")||price.equals("")){
                    Toast.makeText(getContext(),"Update",Toast.LENGTH_LONG).show();
                }
                else {
                    int key = 0;
                    MyDatabase.getInstance(getContext()).bookDao().updateBook(bookName,sizeImg,price,key);

                }
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //System.exit(0);
//                bookName.setText("");
//                sizeImg.setText("");
//                price.setText("");
            }
        });

        return builder.create();
    }

    public FromDialog.DialogCallback callback;
    public void setCallback(FromDialog.DialogCallback callback) {
        this.callback = callback;
    }

    public interface DialogCallback<T>{
        void onClicked(T data);
    }
}
