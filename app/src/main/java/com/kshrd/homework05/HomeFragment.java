package com.kshrd.homework05;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kshrd.homework05.data.MyDatabase;
import com.kshrd.homework05.data.dao.BookDao;
import com.kshrd.homework05.data.entity.Book;
import com.kshrd.homework05.utils.BookAdapter;
import com.kshrd.homework05.utils.FromDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements FromDialog.DialogCallback<String> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView recyclerViewBook;
    RecyclerView.LayoutManager layoutManager;
    BookDao bookDao;
    MyDatabase database;
    BookAdapter bookAdapter;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }
    List<Book>bookList;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);

    }

    private void setup(){
        bookList = new ArrayList<>();
        layoutManager=new LinearLayoutManager(getContext());
        bookAdapter=new BookAdapter(getContext(),MyDatabase.getInstance(getContext()).bookDao().getBooks(),getFragmentManager());
        recyclerViewBook.setHasFixedSize(true);
        recyclerViewBook.setLayoutManager(layoutManager);
        recyclerViewBook.setAdapter(bookAdapter);
        recyclerViewBook.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.HORIZONTAL));

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_option,menu);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        return super.onOptionsItemSelected(item);
        switch (item.getItemId()) {

            case R.id.nav_add:
                AddDialog();
                return true;
            case R.id.nav_search:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void AddDialog() {
        FromDialog fromDialog=new FromDialog();
        fromDialog.show(getFragmentManager(),"add dialog");
        fromDialog.setCallback(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_home, container, false);
          recyclerViewBook = v.findViewById(R.id.rvBook);
          setup();
         return v;
    }

    @Override
    public void onClicked(String data) {
        Log.e( "Tag", "onClicked: "+data );
    }
    private void reload(){
        bookAdapter.reloadItems(bookDao.getBooks());
    }

}