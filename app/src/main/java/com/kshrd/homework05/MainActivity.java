package com.kshrd.homework05;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    FragmentManager fm=getSupportFragmentManager();
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm.beginTransaction().replace(R.id.main_container,new HomeFragment()).commit();
        bottomNavigationView=findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

//                FragmentTransaction ft= fm.beginTransaction();
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_home:
                        fm.beginTransaction()
                                .replace(R.id.main_container,new HomeFragment(),null).addToBackStack(null).commit();
                        return true;
                    case R.id.nav_dashboard:

                        fm.beginTransaction()
                                .replace(R.id.main_container,new DashBoardFragment()).commit();
                        return true;
                    case R.id.nav_notification:
                        fm.beginTransaction()
                                .replace(R.id.main_container,new NotificationFragment()).commit();
                        return true;
                    case R.id.nav_upload:
                        Toast.makeText(getApplicationContext(),"Upload",Toast.LENGTH_LONG).show();
                        return true;
                    case R.id.nav_share:
                        Toast.makeText(getApplicationContext(),"Share",Toast.LENGTH_LONG).show();
                        return true;
                    default:
                }
                return false;
            }

        });
    }

}